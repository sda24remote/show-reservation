import config.HibernateUtil;
import entity.Admin;
import entity.User;
import repository.AdminRepository;
import repository.UserRepository;

public class Main {
    public static void main(String[] args) {


        User user = new User("AA","124","asdsa","123");

        UserRepository userRepository = new UserRepository();

        userRepository.createUser(user);

        Admin admin = new Admin("Andrei","1231","asdsad");

        AdminRepository adminRepository = new AdminRepository();

        adminRepository.createAdmin(admin);

    }
}
