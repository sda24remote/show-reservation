package entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@Data
public class Admin extends Account{

    private int verificationCode;


    public Admin(String username, String password, String email) {
        super(username, password, email);
        this.verificationCode = randomNumber(10,200);
    }

    private int randomNumber(int min, int max) {
        return min + (int)(Math.random() * ((max - min) + 1));
    }

}
