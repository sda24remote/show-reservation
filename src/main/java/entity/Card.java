package entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    private Long serialNumber;

    private String bankName;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Card( String bankName) {
        this.bankName = bankName;
        this.serialNumber = randomNumber(1000000000000000L,9000000000000000L);
    }

    private long randomNumber(long min, long max) {
        return min + (long)(Math.random() * ((max - min) + 1));
    }
}
