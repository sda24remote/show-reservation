package entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class User extends Account {

    private String fullName;

    private Long CNP;

    @OneToMany(mappedBy = "user")
    private List<Card> card;


    public User(String username, String password, String email, String fullName) {
        this.fullName = fullName;
        this.CNP = randomNumber(1000000000000000L,9000000000000000L);
        this.card = new ArrayList<>();
        this.username= username;
        this.password= password;
        this.email= email;
        Account account = new Account(username, password, email);
    }

    private long randomNumber(long min, long max) {
        return min + (long) (Math.random() * ((max - min) + 1));
    }
}
