package repository;

import config.SessionTransaction;
import entity.User;

public class UserRepository extends SessionTransaction {
    public void createUser(User user) {
        openSession();
        session.save(user);
        System.out.println("user saved with success");
        closeSession();
    }
}
