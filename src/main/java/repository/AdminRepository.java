package repository;

import config.SessionTransaction;
import entity.Admin;
import entity.User;

public class AdminRepository extends SessionTransaction {
    public void createAdmin(Admin admin) {
        openSession();
        session.save(admin);
        System.out.println("admin saved with success");
        closeSession();
    }
}
