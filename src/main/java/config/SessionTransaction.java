package config;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class SessionTransaction {
    protected static Session session;
    protected static Transaction transaction;

    // OPEN SESSION /TRANSACTION
    public static void openSession(){
        session = HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
    }

    // CLOSE SESION / TRANSACTION
    public static void closeSession() {
        transaction.commit();
        session.close();
    }
}
